package PJWSTK.Maven;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Season;

public class SeasonAction {
	
	List <Season> seasons = new ArrayList();
	private String addSQL = "INSERT INTO SEASON (SEASON_NUMBER INT, YEAR_OF_RELEASE INT) VALUES (?, ?)";
	private String selectALLSQL = "SELECT * FROM SEASON";
	private String deleteAllSQL = "DELETE FROM SEASON";
	
	public void add(DBConnect connection, Season season) throws SQLException {
		PreparedStatement addActorQuery = connection.getConnection().prepareStatement(addSQL);
		addActorQuery.setInt(1, season.getSeasonNumber());
		addActorQuery.setInt(2, season.getYearOfRelease());
		addActorQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			Season season = new Season();
			season. setId(selectAllQuery.getInt("ID"));
			season.setSeasonNumber(selectAllQuery.getInt("SEASON_NUMBER"));
			season.setYearOfRelease(selectAllQuery.getInt("YEAR_OF_RELEASE"));
			seasons.add(season);
		}
		selectAllQuery.beforeFirst();
		return seasons;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteSeasonsQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteSeasonsQuery.executeUpdate();
	}

}
