package PJWSTK.Maven;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Actor;
import model.Director;
import model.TvSeries;


public class TvSeriesAction {
	
	List <TvSeries> tvseries = new ArrayList();
	private String addSQL = "INSERT INTO TVSERIES (NAME) VALUES (?)";
	private String selectALLSQL = "SELECT * FROM TVSERIES";
	private String deleteAllSQL = "DELETE FROM TVSERIES";
	
	public void add(DBConnect connection, TvSeries tvserie) throws SQLException {
		PreparedStatement addActorQuery = connection.getConnection().prepareStatement(addSQL);
		addActorQuery.setString(1, tvserie.getName());
		addActorQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			TvSeries tvserie = new TvSeries();
			tvserie. setId(selectAllQuery.getInt("ID"));
			tvserie.setName(selectAllQuery.getString("NAME"));
			tvseries.add(tvserie);
		}
		selectAllQuery.beforeFirst();
		return tvseries;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteTvSeriesQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteTvSeriesQuery.executeUpdate();
	}
}
