package PJWSTK.Maven;

import java.util.*;
import model.Director;
import java.sql.*;
import java.sql.Date;

public class DirectorAction {
	
	List <Director> directors = new ArrayList();
	private String addSQL = "INSERT INTO DIRECTOR (NAME, DATE_OF_BIRTH, BIOGRAPHY) VALUES (?, ?, ?)";
	private String selectALLSQL = "SELECT * FROM DIRECTOR";
	private String deleteAllSQL = "DELETE FROM DIRECTOR";
		
	public void add(DBConnect connection, Director director) throws SQLException {
		PreparedStatement addDirectorQuery = connection.getConnection().prepareStatement(addSQL);
		addDirectorQuery.setString(1, director.getName());
		addDirectorQuery.setDate(2, Date.valueOf(director.getDateOfBirth()));
		addDirectorQuery.setString(3, director.getBiography());
		addDirectorQuery.executeUpdate();
	}
	
	public List selectAll(DBConnect connection) throws SQLException {
		Statement statement = connection.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet selectAllQuery = statement.executeQuery(selectALLSQL);
		selectAllQuery.beforeFirst();
		while(selectAllQuery.next()) {
			Director director = new Director();
			director. setId(selectAllQuery.getInt("ID"));
			director.setName(selectAllQuery.getString("NAME"));
			director.setDateOfBirth(selectAllQuery.getDate("DATE_OF_BIRTH").toLocalDate());
			director.setBiography(selectAllQuery.getString("BIOGRAPHY"));
			directors.add(director);
		}
		selectAllQuery.beforeFirst();
		return directors;
	}
	
	public void deleteAll(DBConnect connection) throws SQLException {
		PreparedStatement deleteDirectorsQuery = connection.getConnection().prepareStatement(deleteAllSQL);
		deleteDirectorsQuery.executeUpdate();
	}

}
